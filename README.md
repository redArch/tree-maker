# Tree Maker

Simple python script to generate images of tree diagrams using simple text input. Originally hacked together in a night, it has been refactored a bit, but still needs improvement in code quality and functionality.

Example 1

`treemaker.py '<program>^[<assign>^[<id>^A, <exp>^<term>^[<term>^<factor>^(<exp>)^[<exp>^<term>^<factor>^<id>^A, +, <term>^<factor>^<id>^B], *, <factor>^<id>^C]]]'`

<img src="examples/example_1.png" alt="example 1 image" width="300" />

Example 2

`treemaker.py '<assign>^[<id>^a, :=, <exp>^[a, +, <term>^<factor>^<power>^[(, <exp>^<term>^[<term>^<factor>^<power>^<id>^b, *, <factor>^<power>^[(, <exp>^<term>^<factor>^<power>^[<id>^c, v, <factor>^[<power>^<id>^d, v, <factor>^<power>^<id>^a]], )]], )]]]'`

<img src="examples/example_2.png" alt="example 2 image" width="300" />

Example 3

`treemaker.py '100^[57^[32^[16, 16], 12^[6, 6], 13^[6, 6, 1]], 43^[7^[3, 3, 1], 9^[4, 4, 1], 17^[8, 8, 1], 10^[5, 5]]]'`

<img src="examples/example_3.png" alt="example 3 image" width="800" />

#!/usr/bin/python3

import sys

# library used to draw text and lines
from PIL import Image, ImageDraw, ImageFont

# nodes of the tree to be drawn
class Node:

	# initializer
	#
	# value is the text of the node
	#
	# font is the font that will be used to draw the text,
	# but here it used to get the width of the text/node
	#
	# level is the depth of the node in the tree (starting at ?0? for the root)
	#
	# parent is the parent node in the tree
	def __init__(self, value, font, level, parent):

		# set value and level
		self.value = value
		self.level = level

		# element width/height and actual width/height are initially equal
		# but elementWidth/Height may be modified later depending on the
		# size of the children nodes and if they overlap other nodes
		self.elementWidth = self.width = font.getsize(value)[0]
		self.elementHeight = self.height = font.getsize(value)[1]

		# initialize children variable as an empty array
		self.children = []

		# set parent
		self.parent = parent

		# initialize position x and y coordinates
		self.x = 0
		self.y = 0

		# initialize sibling variable
		self.leftSibling = None
		self.rightSibling = None

	# add a node as a child of self
	def addChild(self, child):

		self.children.append(child)

# confirm input is valid
def checkInput(userInput):

	# position of current character
	# start at 1 to more accurately
	# represent the position of the
	# character to the user
	charPos = 1

	# keep track of opening/closing brackets
	# used as a stack
	brackets = []

#    if bool(re.compile(r'[^A-z0-9.[],:<>]').search(data)):
#        print("Invalid characters in string")
#        exit()

	# loop through each character "c" in user input
	for c in userInput:

		# opening bracket found, append it to brackets
		if c == '[':

			# push to the end of the stack
		    brackets.append(c)

			# next character after current should not be another bracket "[["
		    if userInput[charPos] == '[':
		        print("A node should not be an array (extra opening bracket) at char:" + str(charPos))
		        exit()
			# node name was not specified
		    elif userInput[charPos] == '^':
		        print("Cannot have an empty node (no name) at char:" + str(charPos))
		        exit()

		# closing bracket found, pop brackets
		elif c == ']':

			# make sure there is an opening bracket to pop
		    if len(brackets) > 0:
		    	# pop from the end of the stack
		        brackets.pop()

			# no opening bracket to pop
		    else:
		        print("Error extra closing bracket at char:" + str(charPos))
		        exit()

		elif c == ',':
		    if userInput[charPos] == '[':
		        print("Error, array without node at " + str(charPos))
		        exit()
		    elif userInput[charPos] == ']':
		        print("Error, empty object at char:" + str(charPos))
		        exit()
		    elif userInput[charPos] == '^':
		        print("Cannot have an empty node (no name) at char:" + str(charPos))
		        exit()

		if c == '^':
		    if userInput[charPos] == ',':
		        print("Cannot have an empty node (no name) at char:" + str(charPos))
		        exit()

		# increment current character position
		charPos = charPos + 1

	# if brackets contains any elements then a closing bracket has been missed
	if len(brackets) > 0:
		print("Error, missing a closing bracket")
		exit()

# returns an array of strings that represent
# the children of the data given
# eg data = 'hello^[w,o,r,l^[d,hello]]'
# would return {'w', 'o', 'r', 'l^[d,hello]'}
def getChildrenInput(data):

	# array to track brackets to ensure that only commas
	# not surrounded by brackets are used as a seperator
	# for children
	brackets = []

	# array of strings that will hold the
	#  elements raw input to be parsed
	elements = []

	# current character position in data
	charPos = 0

	# start of current element
	currentElementStart = 0

	# single child return whole string
	if data[0] != '[': return [data]
	# remove first and last character from data
	# data becomes data[1] to data[len(data) - 1]
	else: data = data[1:len(data) - 1]

	# loop through all characters in data
	for c in data:

		# opening bracket found, append it to brackets
		if c == '[': brackets.append('[')
		# closing bracket found, pop brackets
		elif c == ']': brackets.pop()
		# comma found check if it is a valid child
		elif c == ',':

			# if brackets is empty child is valid
		    if len(brackets) < 1:

				# add the child, all text from the
				# last comma to the current one
		        elements.append(data[currentElementStart:charPos])

		        # set the new start for the next element
		        currentElementStart = charPos + 1

		# increment the current character position
		charPos = charPos + 1

	# append the final element
	elements.append(data[currentElementStart:charPos])

	return elements

# userInput is the input from the user,
# though it may only be a portion of the user
# input when getting the children nodes
#
# font is the font object that will be used
# to draw the text of the nodes
#
# level is the depth of the node in the tree
# root node starts at depth 0
#
# parent is the parent node of the current input
#
# siblingLevels is a 2D array of nodes with the first
# index representing the level (depth) of the nodes
# and the second index representing the inorder position
# of the nodes in that row
def parseInput(userInput, font, level = 0, parent = None, siblingLevels = []):

	# create node with name using the input up to the children indicator or use all input if no children indicator
	rootNode = Node(userInput[0:userInput.find('^')], font, level, parent) if userInput.find('^') != -1 else Node(userInput, font, level, parent)

	# the array for this level does not exist in siblingLevels, so
	# create it with the current node as the first node in the level
	if len(siblingLevels) <= level:

		# append an array with rootNode as
		# the first and only element
		siblingLevels.append([rootNode])

	# the array for this level exists so, there is atleaset one element
	# that can be used as a left sibling for the appended node
	else:

		# add the current node to the siblingsLevels
		# array at the correct level
		siblingLevels[level].append(rootNode)

		# set current node's left sibling
		# left sibling is the second last node in the current leve
		rootNode.leftSibling = siblingLevels[level][len(siblingLevels[level]) - 2]

		# set current node's leftSibling's right sibling as the current node
		rootNode.leftSibling.rightSibling = rootNode

	# if the current input has no children then return the current root
	if userInput.find('^') == -1: return rootNode

	# create an array of children input to be parsed using parseInput
	childrenInput = getChildrenInput(userInput[userInput.find('^') + 1:len(userInput)])

	# loop through childrenInput and add them to rootNode as child nodes
	for childInput in childrenInput: rootNode.addChild(parseInput(childInput, font, level + 1, rootNode))

	# return the root node
	return rootNode

# recursive algorithm that will calculate the width
# and height of a node with all of it's children
def calculateChildrenSizeRecursive(root, height = 0):

	# if terminal node return real width
#    if len(root.children) == 0:

	# else node has children
#    else:

	# determine the largest single width of the children
	if height == 0: height = root.height

	childMaxWidth = 0
	childMinWidth = root.width

	for child in root.children:

		child.height = height

		childSize = calculateChildrenSizeRecursive(child, height)

		if  childSize[0] > childMaxWidth: childMaxWidth = childSize[0]
		elif childSize[0] < childMinWidth: childMinWidth = childSize[0]

	childMaxWidth = childMaxWidth if childMaxWidth > childMinWidth * 1.5 else childMinWidth * 1.5

	root.elementWidth = childMaxWidth

	return [root.width, root.height]



#        childrenMaxSize = [childMaxSize[0], childMaxSize[1]]

		# if the width of all the children (assuming they're all the same size as the largest one) is greater than the real width of the root node,
		# then the element width of the root node should be that, else it should be it's real width
#        return [childrenMaxSize[0] if childrenMaxSize[0] > root.width else root.width, childrenMaxSize[1] if childrenMaxSize[1] > root.height else root.height]

#        if childrenMaxSize[0] > root.realWidth and childrenMaxSize[1] > root.realHeight:
#            return childrenMaxSize
#        elif childrenMaxSize[0] > root.realWidth:
#        	return [childrenMaxSize[0], root.realHeight]
#        elif childrenMaxSize[1] > root.realHeight
#        else:
#            return [root.realWidth, childMaxSize[1]]

def drawNodes(root, draw):

	draw.text((root.x - (root.width / 2), root.y), root.value, fill=(0, 0, 0), font=font)

	for child in root.children:

		draw.line([(root.x, root.y + root.height), (child.x, child.y)], (0, 0, 0), 5)
		drawNodes(child, draw)


def calculateCoordinates(root, x = 0, y = 0):

	calculateCoordinatesRecursive(root, x, y)

	while not fixOverLapRecursive(root): calculateCoordinatesRecursive(root, x, y)

def calculateCoordinatesRecursive(root, x, y):

	root.x = x
	root.y = y

	childNumber = 0

	for child in root.children:

		calculateCoordinatesRecursive(child, x + (root.elementWidth * (childNumber - (len(root.children) - 1) / 2)), y + child.height * 3)
		childNumber = childNumber + 1

def fixOverLapRecursive(root):

	for child in root.children:

		if not fixOverLapRecursive(child): return False

	if root.leftSibling != None:

		#print("LeftSibling")

		leftSibling = root.leftSibling

		#print('root: ' + root.value + ', leftSibling: ' + root.value)

#        if root.parent is leftSibling.parent: return True

		spaceCurrent = (root.x - root.width / 2) - (leftSibling.x + leftSibling.width / 2)
		spaceWanted = root.height * 1.5

		if spaceCurrent < spaceWanted:

		    parent1 = root.parent
		    parent2 = leftSibling.parent

		    while parent1 is not parent2:

		        parent1 = parent1.parent
		        parent2 = parent2.parent

		    parent1.elementWidth = parent1.elementWidth + spaceWanted - spaceCurrent
		    #print("Child width changed")

		    return False

		else:

		    return True

	else:

		return True

def center(root):

	minLeft = leftMinRecursive(root)
	maxRight = rightMaxRecursive(root)

#	print("MIN: " + str(minLeft))
#	print("MAX: " + str(maxRight))

#    leftChild = None if len(root.children) < 1 else root.children[0]
#    rightChild = None if len(root.children) < 1 else root.children[len(root.children) - 1]

#    while leftChild is not None:

#        print(minLeft)
#        minLeft = min(minLeft, leftChild.x - leftChild.width / 2)
#        leftChild = None if len(leftChild.children) < 1 else leftChild.children[0]

#    print(minLeft)

#    while rightChild is not None:

#        print(maxRight)
#        maxRight = max(maxRight, rightChild.x + rightChild.width / 2)
#        rightChild = None if len(rightChild.children) < 1 else rightChild.children[len(rightChild.children) - 1]

#    print(maxRight)

	calculateCoordinates(root, -1 * minLeft)

	return maxRight - minLeft

def leftMinRecursive(root):

	minRoot = root.x - root.width / 2

	for child in root.children:

		minRoot = min(minRoot, leftMinRecursive(child))

	return minRoot

def rightMaxRecursive(root):

	maxRoot = root.x + 	root.width / 2

	for child in root.children:

		maxRoot = max(maxRoot, rightMaxRecursive(child))

	return maxRoot

def nodeChildrenDepth(root):

	if len(root.children) < 1: return 0

	maxChildDepth = 0

	for child in root.children:

		    maxChildDepth = max(maxChildDepth, nodeChildrenDepth(child))

	return maxChildDepth + 1

# DEBUG function
# print out info about tree structure
def printNodes(children):

	nextChildren = []

	for child in children:
		#draw.text((0,0), child.value, fill=(255, 0, 0), font=font)
		print('{' + child.value + ': width = ' + str(child.width) + ', x = ' + str(child.x) + ', y = ' + str(child.y), end='}')
		#print('{' + child.value + ': level = ' + str(child.level), end='}')
		nextChildren.extend(child.children)

	print()

	if len(nextChildren) > 0:
		printNodes(nextChildren)


if (len(sys.argv) >= 2):

	# font object
	font = ImageFont.truetype("Inconsolata-Regular.ttf", 128)

	# remove spaces
	userInput = sys.argv[1].replace(" ", "")

	# check input
	checkInput(userInput)

	# parseInput to root
	root = parseInput(userInput, font)

	calculateChildrenSizeRecursive(root)

	calculateCoordinates(root)

	#printNodes([root])

	# img object
	img = Image.new('RGB', (round(center(root)), root.height * (nodeChildrenDepth(root) * 3 + 1)), color = (255, 255, 255))

	# draw object of the img
	draw = ImageDraw.Draw(img)

	drawNodes(root, draw)

	img.save('pil_text.png')
